<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>CRUD - Edit</title>
  </head>
  <body>

    <div class="container-fluid">
        <div class="container" style="background-color: rgb(241, 241, 241)">
            <div class="row">
                <div  class="col-12" style="background-color: rgb(234, 237, 255)" >
                    <h1 style="color: black" class="text-center pt-3 ">CRUD KARYAWAN</h1>
                    <p class="text-center mt-0"><strong>- Wahyu Rudiartha -</strong></p>
                </div>
                
                <div class="navbar navbar-expand-sm navbar-dark bg-dark px-2">
                  <div class="container">
                    <div class="collapse navbar-collapse">
                      <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="/registrasi">Registrasi</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="/daftar">Daftar Karyawan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/daftar">Edit Karyawan</a>
                          </li>
                      
                    </div>
                  </div>
                </div>
           
          </div>
            <br>
            
            <div class="col-11 mx-4" >
              <p>Silahkan edit form karyawan dibawah !</p>
              <div class="card">
                <div class="card-header bg-primary text-white">
                  Edit Data Karyawan
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-8">
                        
                        @foreach ($kry as $item)
                        <form action="/simpanUpdate" method="POST">
                          {{ csrf_field() }}
                            <div class="mb-3">
                                
                                <input type="hidden" class="form-control" name="id" value="{{$item->id}}">
                            </div>

                            <div class="mb-3">
                              <label for="nama-karyawan" class="form-label">Nama Karyawan</label>
                              <input type="text" class="form-control" name="nama_karyawan" value="{{$item->nama_karyawan}}">
                            </div>
        
                            <div class="mb-3">
                                <label for="no-karyawan" class="form-label">No Karyawan</label>
                                <input type="number" class="form-control" name="no_karyawan" value="{{$item->no_karyawan}}">
                            </div>
        
                            <div class="mb-3">
                                <label for="nohp-karyawan" class="form-label">No HP Karyawan</label>
                                <input type="text" class="form-control" name="no_telp_karyawan" value="{{$item->no_telp_karyawan}}">
                            </div>
        
                            <div class="mb-3">
                                <label for="jabatan-karyawan" class="form-label">Jabatan Karyawan</label>
                                <select type="text" class="form-select" name="jabatan_karyawan" placeholder="Input jabatan">
                                  <option selected>{{$item->jabatan_karyawan}}</option>
                                  <option value="Direktur Utama">Direktur Utama</option>
                                  <option value="Direktur">Direktur</option>
                                  <option value="Manajer">Manajer</option>
                                  <option value="Staff">Staff</option></select>
                            </div>
        
                            <div class="mb-3">
                                <label for="divisi-karyawan" class="form-label">Divisi Karyawan</label>
                                <select type="text" class="form-select" name="divisi_karyawan" placeholder="Input divisi">
                                  <option selected>{{$item->divisi_karyawan}}</option>
                                  <option value="Divisi Produksi">Divisi Produksi</option>
                                  <option value="Divisi Pemasaran">Divisi Pemasaran</option>
                                  <option value="Divisi Personalia">Divisi Personalia</option>
                                  <option value="Divisi Umum">Divisi Umum</option>
                                  <option value="Divisi IT">Divisi IT</option>
                                </select>
                            </div><br>
                          
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="/daftar" class="btn btn-danger">Batal</a>
                          </form>
                          @endforeach
                    </div>
        
                </div>
                </div>
              </div><br><br><br><br><br>
            </div>
            

            
        </div>
    <br><br><br><br><br><br><br><br><br><br><br><br></div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>