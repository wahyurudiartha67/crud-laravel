<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/style.css">
    <title>CRUD - Daftar</title>

  </head>
  <body>
    <div class="container-fluid">
        <div class="container" style="background-color: rgb(241, 241, 241)">
            <div class="row">
              <div  class="col-12" style="background-color: rgb(234, 237, 255)" >
                  <h1 style="color: black" class="text-center pt-3 ">CRUD KARYAWAN</h1>
                  <p class="text-center mt-0"><strong>- Wahyu Rudiartha -</strong></p>
              </div>
              
              <div class="navbar navbar-expand-sm navbar-dark bg-dark px-2">
                <div class="container">
                  <div class="collapse navbar-collapse">
                    <ul class="navbar-nav">
                      <li class="nav-item">
                        <a class="nav-link" href="/">Home</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="/registrasi">Registrasi</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link active" href="/daftar">Daftar Karyawan</a>
                      </li>
                    
                  </div>
                </div>
              </div>
       
      </div>
            
          <div class="row">
            <div class="col-11 mx-4">
              <br>
              <p>Berikut merupakan daftar karyawan yang teregistrasi</p>
              <a href="/registrasi" type="button" class="btn btn-outline-primary">Tambah Data</a>
              <div class="card">
                <div class="card-header bg-success text-white">
                  Daftar Karyawan
                </div>
                <div class="card-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nama Karyawan</th>
                        <th scope="col">No Karyawan</th>
                        <th scope="col">No Telp Karyawan</th>
                        <th scope="col">Jabatan Karyawan</th>
                        <th scope="col">Divisi Karyawan</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($kry as $item)
                      <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->nama_karyawan }}</td>
                        <td>{{ $item->no_karyawan }}</td>
                        <td>{{ $item->no_telp_karyawan }}</td>
                        <td>{{ $item->jabatan_karyawan }} </td>
                        <td>{{ $item->divisi_karyawan }} </td>
                        <td>
                          <a href="/update/{{$item->id}}" class="btn btn-warning ">Edit</a>
                          <a onclick="return confirm('Yakin ingin menghapus data ?')" href="/delete/{{$item->id}}" class="btn btn-danger">Hapus</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div><br><br><br><br><br><br><br>

              
            </div>
          </div>
         



        </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>