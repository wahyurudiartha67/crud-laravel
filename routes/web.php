<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\registrasiController;
use App\Http\Controllers\daftarController;
use App\Http\Controllers\updateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/registrasi',[registrasiController::class,'show']);

Route::get('/daftar',[daftarController::class,'show']);

Route::post('/simpan',[registrasiController::class,'prosesSimpan']);

Route::get('/update/{id}',[updateController::class,'edit']);

Route::post('/simpanUpdate',[updateController::class,'prosesUpdate']);

Route::get('/delete/{id}',[updateController::class,'prosesHapus']);
