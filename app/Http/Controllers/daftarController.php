<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class daftarController extends Controller
{
    public function show(){

        $karyawan = DB::table('karyawan')->get();

        return view ('daftar',['kry'=>$karyawan]);
    }
}
